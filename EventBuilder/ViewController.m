//
//  ViewController.m
//  EventBuilder
//
//  Created by Aleksis Kavalieris on 19/01/17.
//  Copyright © 2017 Runawayplay. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
