//
//  AppDelegate.m
//  EventBuilder
//
//  Created by Aleksis Kavalieris on 19/01/17.
//  Copyright © 2017 Runawayplay. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
