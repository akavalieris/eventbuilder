//
//  AppDelegate.h
//  EventBuilder
//
//  Created by Aleksis Kavalieris on 19/01/17.
//  Copyright © 2017 Runawayplay. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

