//
//  main.m
//  EventBuilder
//
//  Created by Aleksis Kavalieris on 19/01/17.
//  Copyright © 2017 Runawayplay. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
